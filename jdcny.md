At our [New York datacamp](https://scraperwiki.com/events/jdcny/),
we set out to liberate data, teach people to liberate data,
and find stories in data.

About 100 people showed up for the event, and
about 40 of them attended the Learn to Scrape sessions.

The hacking was punctuated by talks by
Tom Lee of the [Sunlight Foundation](http://www.sunlightfoundation.com)
and Jake Porway of [Data Without Borders](http://datawithoutborders.cc)

Projects
---------------
[Dan Nguyen](https://twitter.com/#!/dancow) scraped Florida mugshots from and used [face.com](http://face.com)'s API to analyse each photo to tell you the arrestee's mood.

[Michael Keller](https://twitter.com/#!/mhkeller),
[Marc Georges](https://twitter.com/#!/marcgeorges) et al.
related the NYPD [stop, question and frisk data](http://www.nyc.gov/html/nypd/html/analysis_and_planning/stop_question_and_frisk_report.shtml)
nine mosques referenced in an NYPD report on surveillance
in order to see whether there had been unusual changes
in stopping activity around these mosques.

The dataset is insanely messy, but they fortunately had access to a relatively clean version that Data Without Borders had [developed](http://wiki.datawithoutborders.cc/index.php?title=Project:Current_events:NYC_DD:NYCLU) in November.

They were still going strong
[after](https://twitter.com/#!/mhkeller/status/165937116671524865) the data camp.
[Refusing to leave](https://twitter.com/#!/mhkeller/status/166008574621913092),
they moved to a [different](https://twitter.com/#!/SusanEMcG/status/165950415945478148)
to a different room after getting kicked out of the data camp space.

[Mike Caprio](https://twitter.com/mik3cap/) and team cleaned a spreadsheet
of 80,000 records from the New York lobbyist website to power a site on New
York lobbyists based on the [Chicago Lobbyists site](http://chicagolobbyists.org)
It appears that $120 million was spend on New York on lobbiests in 2011.

I helped one team relate contracts from
[Open Book New York](http://scraperwiki.com/scrapers/open_book_new_york)
to data that they had scraped by hand (!) from hand-written forms
in order to identify pontential conflicts of interest.

I helped another team identify potential stories (outliers) in the
[NYC Open Data graffiti locations dataset](http://nycopendata.socrata.com/Other/Graffiti-Locations/2j99-6h29). <!-- More on this in some other blog post -->

[Susan McGregor](https://twitter.com/SusanEMcG)
was ["clearly hooked"](https://twitter.com/#!/SusanEMcG/status/166346113014706178)
because she liberated [lobbyist contract details](http://www.nyc.gov/lobbyistsearch/)
the next evening
[instead of watching the Superbowl](https://twitter.com/#!/SusanEMcG/status/166354779499536385).

Technical Awards
------------------

[Mike Caprio](https://twitter.com/mik3cap/)
won *Best Data Liberator*
for [liberating](https://scraperwiki.com/scrapers/iowa_accident_reports/)
the [Iowa accident reports database]().

[Michelle Koeth](https://twitter.com/michellekoeth)
won *Best Creation of an API* for [scraping](https://scraperwiki.com/scrapers/hospitalcompare/)
New York, NY hospitals from [Medicare Hospital Compare](http://www.hospitalcompare.hhs.gov/hospital-search.aspx)

Jeremy Baron, from UN peacekeeping team, won *Best Use of ScraperWiki* for scraping
[United Nations PDFs](https://scraperwiki.com/scrapers/un-memstates-contribs-toregbudget-2012/).
This team also scraped peacekeeping 
[statistics](http://scraperwiki.com/scrapers/un_peacekeeping_statistics)
and [contributions](https://scraperwiki.com/scrapers/un_resolution_55235_peacekeeping_contributions/)


Honorary ScraperWikian
----------------------
Susan McGregor was awarded *Honorary ScraperWikian*.
We haven't decided what that means yet. :)

Learning
--------------
Teaching the Learn to Scrape sessions and working with many of the project teams, I got
the impression that we had opened participants to thinking more about how data can be scraped,
transformed and analyzed to identify unusual subsets and potential stories.
<!-- ([split-applied-combined](http://plyr.had.co.nz/), if you will) -->

Our Learn to Scrape sessions seemed to work as well; I found several participants
who had claimed no knowledge of webscraping prior to the sessions
to be creating reasonably complex scrapers by the next afternoon.

What Next? 
-------------
[More data camps](https://scraperwiki.com/events/) are coming up,
and several groups plan on contining to work on their projects.
But in the mean time, we now have
[lots](https://scraperwiki.com/scrapers/nyc_school_budgets/)
of
[data](https://scraperwiki.com/scrapers/nyc_lobbyist_directory_browser/)
for you to analyze!
