We've blogged previously about
[created](http://blog.scraperwiki.com/2011/11/14/with-tools-tables-and-tours-were-looking-to-liberate-data-across-the-us/)
[maps](http://blog.scraperwiki.com/2011/11/25/mapping-tahrirsupplies/)
[from](http://blog.scraperwiki.com/2011/12/09/scraping-the-protests/)
scraped data.
You might be surprised by how easy it is to make them; check
out [this tutorial](https://scraperwiki.com/docs/html/html_views/)
that Zarino just put together.
