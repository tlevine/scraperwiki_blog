I installed some new Python libraries and restructured the Python libraries
[documentation page](https://scraperwiki.com/docs/python/python_libraries/).
Some highlights

* Gensim is "Topic Modelling for Humans".
Read the [introduction](http://radimrehurek.com/gensim/intro.html) to the documentation.
I'm looking for an excuse to play with it.

* [unidecode](http://pypi.python.org/pypi/Unidecode)
transliterates Unicode into ASCII. It's helpful for
things like making column names.

* [Beautiful Soup 4 beta](http://www.crummy.com/software/BeautifulSoup/)
(It's a separate package from Beautiful Soup 3, so Beautiful Soup 3 still works.)

<!--
* [pyparsley](https://github.com/fizx/pyparsley) is the Python
client to [parsley](https://github.com/fizx/parsley/wiki), which
"is a simple language for extracting structured data from web pages."
Read the [documentation](https://github.com/fizx/parsley/wiki)
and an interesting discussion on [Hacker News](http://news.ycombinator.com/item?id=1584597)).
-->
